package ch.ethz.matsim.papers.paris_paper.av;

import org.matsim.core.utils.misc.Time;

import ch.ethz.matsim.av.routing.AVRoute;

public class ParisAVRoute extends AVRoute {
	private double waitingTime = Time.getUndefinedTime();

	public ParisAVRoute(AVRoute route) {
		super(route);
	}

	public double getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(double waitingTime) {
		this.waitingTime = waitingTime;
	}
}