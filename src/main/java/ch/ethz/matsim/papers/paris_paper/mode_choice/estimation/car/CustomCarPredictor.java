package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.car;

import java.util.List;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.population.routes.NetworkRoute;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.TripStructureUtils.Trip;
import org.matsim.facilities.ActivityFacilities;

import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;

public class CustomCarPredictor {
	final private TripRouter router;
	final private Network network;

	public CustomCarPredictor(TripRouter router, ActivityFacilities facilities, Network network) {
		this.router = router;
		this.network = network;
	}

	public CustomCarPrediction predict(ModeChoiceTrip trip) {
		Trip tripInformation = trip.getTripInformation();

		LinkWrapperFacility originFacility = new LinkWrapperFacility(
				this.network.getLinks().get(tripInformation.getOriginActivity().getLinkId()));
		LinkWrapperFacility destinationFacility = new LinkWrapperFacility(
				this.network.getLinks().get(tripInformation.getDestinationActivity().getLinkId()));

		List<? extends PlanElement> result = router.calcRoute("car", originFacility, destinationFacility,
				tripInformation.getOriginActivity().getEndTime(), trip.getPerson());

		NetworkRoute route = (NetworkRoute) ((Leg) result.get(0)).getRoute();
		return new CustomCarPrediction(route.getDistance(), route.getTravelTime(), route);
	}
}
