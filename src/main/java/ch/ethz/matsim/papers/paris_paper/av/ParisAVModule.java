package ch.ethz.matsim.papers.paris_paper.av;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.contrib.dvrp.router.DvrpRoutingNetworkProvider;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.router.NetworkRoutingModule;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.LeastCostPathCalculatorFactory;
import org.matsim.core.router.util.TravelDisutility;
import org.matsim.core.router.util.TravelTime;

import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVGeneratorConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.framework.AVUtils;
import ch.ethz.matsim.papers.paris_paper.av.waiting_time.AVWaitingTime;
import ch.ethz.matsim.papers.paris_paper.av.waiting_time.ConstantAVWaitingTime;
import ch.ethz.matsim.papers.paris_paper.av.waiting_time.estimation.MovingAverageWaitingTime;

public class ParisAVModule extends AbstractModule {
	private final long fleetSize;
	private double constantWaitingTime;
	private double stuckWaitingTime = 3600.0;

	public ParisAVModule(long fleetSize, double constantWaitingTime) {
		this.fleetSize = fleetSize;
		this.constantWaitingTime = constantWaitingTime;
	}

	@Provides
	@Singleton
	public MovingAverageWaitingTime provideAVWaitingTime(@Named(AVModule.AV_MODE) Network network) {
		double estimationStartTime = 5.0 * 3600.0;
		double estimationEndTime = 22.0 * 3600.0;
		double estimationInterval = 1200.0;

		double defaultWaitingTime = 3.0 * 60.0;
		int horizon = 10;

		return new MovingAverageWaitingTime(estimationStartTime, estimationEndTime, estimationInterval, horizon,
				defaultWaitingTime, stuckWaitingTime, network);
	}

	@Provides
	@Singleton
	public ConstantAVWaitingTime provideConstantAVWaitingTime() {
		return new ConstantAVWaitingTime(constantWaitingTime);
	}

	@Override
	public void install() {
		addRoutingModuleBinding("av").to(ParisAVRoutingModule.class);
		bind(StageActivityTypes.class).toInstance(new StageActivityTypesImpl("pt interaction"));

		AVUtils.registerGeneratorFactory(binder(), "ParisGenerator", ParisGenerator.Factory.class);

		AVConfig config = new AVConfig();
		AVOperatorConfig operatorConfig = config.createOperatorConfig("av");

		operatorConfig.createDispatcherConfig("SingleHeuristic");
		AVGeneratorConfig generatorConfig = operatorConfig.createGeneratorConfig("ParisGenerator");
		generatorConfig.setNumberOfVehicles(fleetSize);

		operatorConfig.createPriceStructureConfig();

		bind(AVConfig.class).toInstance(config);

		bind(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)))
				.toProvider(DvrpRoutingNetworkProvider.class).asEagerSingleton();

		bind(Key.get(Network.class, Names.named(AVModule.AV_MODE)))
				.to(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)));

		if (Double.isNaN(constantWaitingTime)) {
			bind(AVWaitingTime.class).to(MovingAverageWaitingTime.class);
			addEventHandlerBinding().to(MovingAverageWaitingTime.class);
		} else {
			bind(AVWaitingTime.class).to(ConstantAVWaitingTime.class);
		}
	}

	@Provides
	@Named("av_base")
	public RoutingModule provideBaseRoutingModule(Population population, @Named(AVModule.AV_MODE) Network network,
			@Named("car") TravelTime travelTime, LeastCostPathCalculatorFactory routerFactory) {
		TravelDisutility travelDisutility = new OnlyTimeDependentTravelDisutility(travelTime);
		LeastCostPathCalculator router = routerFactory.createPathCalculator(network, travelDisutility, travelTime);
		return new NetworkRoutingModule(AVModule.AV_MODE, population.getFactory(), network, router);
	}
}
