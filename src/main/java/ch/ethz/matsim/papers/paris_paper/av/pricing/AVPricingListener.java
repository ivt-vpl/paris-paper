package ch.ethz.matsim.papers.paris_paper.av.pricing;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;

import ch.ethz.matsim.av_cost_calculator.CostCalculatorExecutor;
import ch.ethz.matsim.av_cost_calculator.run.ParameterBuilder;
import ch.ethz.matsim.av_cost_calculator.run.PricingAnalysisHandler;

public class AVPricingListener implements IterationEndsListener {
	private final PricingAnalysisHandler handler;
	private final CostCalculatorExecutor executor;

	private final double scenarioScale;
	private final int horizon;

	private List<Double> history = new LinkedList<>();

	private final BufferedWriter writer;

	private double activePrice = 0.0;

	public AVPricingListener(double scenarioScale, int horizon, PricingAnalysisHandler handler,
			CostCalculatorExecutor executor, OutputDirectoryHierarchy output) throws IOException {
		this.scenarioScale = scenarioScale;
		this.horizon = horizon;

		this.handler = handler;
		this.executor = executor;

		writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(output.getOutputFilename("prices.csv"))));
		writer.write("iteration;active_price;computed_price\n");
		writer.flush();

		for (int i = 0; i < horizon; i++) {
			history.add(0.0);
		}
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		double totalTime = 24.0 * 3600.0;

		Map<String, String> parameters = new ParameterBuilder(scenarioScale, totalTime, "Solo_Berlin").build(handler);
		double price = executor.computePricePerPassengerKm(parameters);
		handler.resetHandler();

		if (!Double.isNaN(price)) {
			history.remove(0);
			history.add(price);
		}

		activePrice = history.stream().mapToDouble(d -> d).sum() / (double) horizon;

		try {
			writer.write(String.format("%d;%f;%f\n", event.getIteration(), activePrice, price));
			writer.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public double getPricePerKm() {
		return activePrice;
	}
}
