package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.pt.subscription;

public class SubscriptionInformation {
	final public boolean hasSubscription;

	public SubscriptionInformation(boolean hasSubscription) {
		this.hasSubscription = hasSubscription;
		
	}
}
