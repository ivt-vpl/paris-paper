package ch.ethz.matsim.papers.paris_paper.av;

import java.util.Collections;
import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.population.PopulationUtils;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.facilities.Facility;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.data.AVOperator;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.replanning.AVOperatorChoiceStrategy;
import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.papers.paris_paper.av.waiting_time.AVWaitingTime;

public class ParisAVRoutingModule implements RoutingModule {
	private final RoutingModule baseRoutingModule;
	private final AVOperatorChoiceStrategy choiceStrategy;
	private final AVRouteFactory routeFactory;
	private final AVWaitingTime waitingTime;

	private final Network network;

	@Inject
	public ParisAVRoutingModule(@Named("av_base") RoutingModule baseRoutingModule,
			AVOperatorChoiceStrategy choiceStrategy, AVRouteFactory routeFactory,
			@Named(AVModule.AV_MODE) Network network, AVWaitingTime waitingTime) {
		this.baseRoutingModule = baseRoutingModule;
		this.choiceStrategy = choiceStrategy;
		this.routeFactory = routeFactory;
		this.network = network;
		this.waitingTime = waitingTime;
	}

	@Override
	public List<? extends PlanElement> calcRoute(Facility<?> fromFacility, Facility<?> toFacility, double departureTime,
			Person person) {
		// Find walk facilities if necessary
		Facility<?> enterFacility = fromFacility;
		Facility<?> leaveFacility = toFacility;

		if (!network.getLinks().containsKey(fromFacility.getLinkId())) {
			enterFacility = new LinkWrapperFacility(NetworkUtils.getNearestLink(network, fromFacility.getCoord()));
		}

		if (!network.getLinks().containsKey(toFacility.getLinkId())) {
			leaveFacility = new LinkWrapperFacility(NetworkUtils.getNearestLink(network, toFacility.getCoord()));
		}

		// Use the vehicle
		Leg carLeg = (Leg) baseRoutingModule.calcRoute(enterFacility, leaveFacility, departureTime, person).get(0);

		Id<AVOperator> operator = choiceStrategy.chooseRandomOperator();
		AVRoute avRoute = routeFactory.createRoute(enterFacility.getLinkId(), leaveFacility.getLinkId(), operator);
		avRoute.setDistance(carLeg.getRoute().getDistance());
		avRoute.setTravelTime(carLeg.getTravelTime());

		ParisAVRoute parisRoute = new ParisAVRoute(avRoute);
		parisRoute.setWaitingTime(waitingTime.getWaitingTime(enterFacility, departureTime));

		Leg leg = PopulationUtils.createLeg(AVModule.AV_MODE);
		leg.setDepartureTime(departureTime);
		leg.setTravelTime(parisRoute.getTravelTime() + parisRoute.getWaitingTime());
		leg.setRoute(parisRoute);

		return Collections.singletonList(leg);
	}

	@Override
	public StageActivityTypes getStageActivityTypes() {
		return new StageActivityTypesImpl();
	}
}
