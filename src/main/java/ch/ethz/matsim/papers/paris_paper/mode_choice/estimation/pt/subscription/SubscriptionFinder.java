package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.pt.subscription;

import org.matsim.api.core.v01.population.Person;

public class SubscriptionFinder {
	public SubscriptionInformation getSubscriptions(Person person) {
		boolean hasSubscription = false;
		if (person.getAttributes().getAttribute("ptSubscription") != null)
			hasSubscription = (boolean) person.getAttributes().getAttribute("ptSubscription");

		return new SubscriptionInformation(hasSubscription);
	}
}
