package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation;

import java.lang.reflect.Field;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.pt.subscription.SubscriptionInformation;

public class CustomModeChoiceParameters {
	// Cost
	public double betaCostBase = -0.206;
	public double averageDistance_km = 40.0;
	public double lambdaDistanceCost = -0.4;

	public double betaCost(double distance_km) {
		distance_km = Math.max(distance_km, 0.001);
		return betaCostBase * Math.pow(distance_km / averageDistance_km, lambdaDistanceCost);
	}

	// AV
	public double alphaAv = 0.0;
	public double betaInVehicleTimeAv_min = -0.017;
	public double betaWaitingTimeAv_min = -0.0484;
	public double distanceCostAv_km = 0.2;

	// Walk
	public double alphaWalk = 1.43;
	public double betaTravelTimeWalk_min = -0.09;

	// Bike
	public double alphaBike = -0.0;
	public double betaTravelTimeBike_min = -0.15;
	public double betaAgeBike_yr = -0.0496;

	// Car
	public double alphaCar = 1.35;
	public double betaTravelTimeCar_min = -0.06;

	public double distanceCostCar_km = 0.2;

	public double parkingSearchTimeCar_min = 4.0;
	public double accessEgressWalkTimeCar_min = 4.0;

	// Public Transport
	public double alphaPublicTransport = 0.0;
	public double betaNumberOfTransfersPublicTransport = -0.17;

	public double betaInVehicleTimePublicTransport_min = -0.017;
	public double betaTransferTimePublicTransport_min = -0.0484;
	public double betaAccessEgressTimePublicTransport_min = -0.0804;

	public double costPublicTransport(SubscriptionInformation subscriptions, double crowflyDistance_km) {
		if (subscriptions.hasSubscription) {
			return 0.0;
		}

		if (crowflyDistance_km < 3.0)
			return 3.0;
		if (crowflyDistance_km < 5.0)
			return 3.0;
		if (crowflyDistance_km < 10.0)
			return 3.5;

		return (int) Math.ceil(crowflyDistance_km / 5.0) * 2.0;
	}

	private static Logger logger = Logger.getLogger(CustomModeChoiceParameters.class);

	public void load(Map<String, String> parameters) {
		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			try {
				Field field = CustomModeChoiceParameters.class.getField(entry.getKey());
				field.set(this, Double.valueOf(entry.getValue()));
				logger.info(String.format("Set %s = %f", entry.getKey(), Double.valueOf(entry.getValue())));
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
