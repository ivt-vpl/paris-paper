package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.other;

import java.util.List;

import ch.ethz.matsim.mode_choice.estimation.DefaultTripCandidate;
import ch.ethz.matsim.mode_choice.estimation.ModalTripEstimator;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;

public class CustomCarPassengerEstimator implements ModalTripEstimator {
	@Override
	public TripCandidate estimateTrip(ModeChoiceTrip arg0, List<TripCandidate> arg1) {
		return new DefaultTripCandidate(0.0, "car_passenger");
	}
}
