package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.av;

import java.util.List;

import ch.ethz.matsim.mode_choice.estimation.ModalTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TripCandidateWithPrediction;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;
import ch.ethz.matsim.papers.paris_paper.av.pricing.AVPricingListener;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.CustomModeChoiceParameters;

public class CustomAvEstimator implements ModalTripEstimator {
	final private CustomAvPredictor predictor;
	final private CustomModeChoiceParameters parameters;
	final private AVPricingListener priceListener;
	final private boolean useDynamicPrice;

	public CustomAvEstimator(CustomModeChoiceParameters parameters, CustomAvPredictor predictor,
			AVPricingListener priceListener, boolean useDynamicPrice) {
		this.predictor = predictor;
		this.priceListener = priceListener;
		this.parameters = parameters;
		this.useDynamicPrice = useDynamicPrice;
	}

	@Override
	public TripCandidate estimateTrip(ModeChoiceTrip trip, List<TripCandidate> preceedingTrips) {
		CustomAvPrediction prediction = predictor.predict(trip);
		double inVehicleTime_min = prediction.inVehicleTime / 60.0;
		double waitingTime_min = prediction.waitingTime / 60.0;
		double distance_km = prediction.distance * 1e-3;

		double cost = parameters.distanceCostAv_km * distance_km;
		double betaCost = parameters.betaCost(distance_km);

		if (useDynamicPrice) {
			cost = priceListener.getPricePerKm() * distance_km;
		}

		double utility = 0.0;
		utility += parameters.alphaAv;

		utility += parameters.betaInVehicleTimeAv_min * inVehicleTime_min;
		utility += parameters.betaWaitingTimeAv_min * waitingTime_min;

		utility += betaCost * cost;

		return new TripCandidateWithPrediction(utility, "av", prediction);
	}
}
