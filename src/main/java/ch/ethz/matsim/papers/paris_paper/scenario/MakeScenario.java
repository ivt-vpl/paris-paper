package ch.ethz.matsim.papers.paris_paper.scenario;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.Node;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.PopulationWriter;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.config.ConfigWriter;
import org.matsim.core.config.groups.ControlerConfigGroup.RoutingAlgorithmType;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ActivityParams;
import org.matsim.core.config.groups.StrategyConfigGroup.StrategySettings;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.network.io.NetworkWriter;
import org.matsim.core.population.algorithms.TripsToLegsAlgorithm;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.MainModeIdentifierImpl;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.facilities.ActivityFacility;
import org.matsim.households.Household;
import org.matsim.households.Households;
import org.matsim.pt.PtConstants;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.pt.transitSchedule.api.TransitScheduleWriter;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Geometry;

import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;

public class MakeScenario {
	private final static Logger logger = Logger.getLogger(MakeScenario.class);

	private final MainModeIdentifier mainModeIdentifier = new MainModeIdentifierImpl();
	private final StageActivityTypes stageActivityTypes = new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE);

	public void run(File scenarioPath, File populationPath) throws MalformedURLException, IOException {
		// Load initial scenario
		Config config = ConfigUtils.loadConfig(scenarioPath.toString() + "/ile_de_france_config.xml");
		config.plans().setInputFile(populationPath.toString());

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		// Load IRIS
		Map<String, IRIS> iris = readIRIS(new File(scenarioPath, "iris/iris.shp"));

		// Prepare network
		Network avNetwork = prepareNetwork(scenario.getNetwork(), iris.values());

		// Prepare schedule
		prepareSchedule(scenario.getTransitSchedule(), iris.values());

		// Prepare population
		preparePopulation(scenario.getPopulation(), scenario.getHouseholds(), avNetwork);

		// Prepare config
		prepareConfig(config);

		// Apply outside
		applyOutside(scenario);

		// Write everything
		new ConfigWriter(config).write(new File(scenarioPath, "paris_config.xml").toString());
		new NetworkWriter(scenario.getNetwork()).write(new File(scenarioPath, "paris_network.xml.gz").toString());
		new PopulationWriter(scenario.getPopulation())
				.write(new File(scenarioPath, "paris_population.xml.gz").toString());
		new TransitScheduleWriter(scenario.getTransitSchedule())
				.writeFile(new File(scenarioPath, "paris_transit_schedule.xml.gz").toString());

		FileUtils.copyFile(new File(scenarioPath, "ile_de_france_facilities.xml.gz"),
				new File(scenarioPath, "paris_facilities.xml.gz"));
	}

	private void preparePopulation(Population population, Households households, Network avNetwork) {
		Map<Id<Person>, Household> memberMap = new HashMap<>();

		for (Household household : households.getHouseholds().values()) {
			for (Id<Person> memberId : household.getMemberIds()) {
				memberMap.put(memberId, household);
			}
		}

		Iterator<? extends Person> personIterator = population.getPersons().values().iterator();

		int totalNumberOfPersons = population.getPersons().size();
		int numberOfProcessedPersons = 0;
		int removedNumberOfPersons = 0;

		while (personIterator.hasNext()) {
			Person person = personIterator.next();
			Plan plan = person.getSelectedPlan();

			new TripsToLegsAlgorithm(stageActivityTypes, mainModeIdentifier).run(plan);
			boolean anyInside = false;

			for (int i = 0; i < plan.getPlanElements().size(); i += 2) {
				Activity activity = (Activity) plan.getPlanElements().get(i);
				Link link = avNetwork.getLinks().get(activity.getLinkId());

				if (link != null) {
					anyInside = true;
					activity.getAttributes().putAttribute("iris", link.getAttributes().getAttribute("iris"));
				}
			}

			if (!anyInside) {
				personIterator.remove();
				removedNumberOfPersons++;
			} else {
				if (memberMap.containsKey(person.getId())) {
					person.getAttributes().putAttribute("carAvailability",
							(String) memberMap.get(person.getId()).getAttributes().getAttribute("carAvailability"));
				}

				for (int i = 0; i < plan.getPlanElements().size(); i += 2) {
					Activity activity = (Activity) plan.getPlanElements().get(i);

					if (activity.getAttributes().getAttribute("iris") == null) {
						activity.setType("outside");
						activity.setLinkId(Id.createLinkId("outside"));
						activity.setFacilityId(Id.create("outside", ActivityFacility.class));
					}
				}

				for (int i = 1; i < plan.getPlanElements().size(); i += 2) {
					Activity previousActivity = (Activity) plan.getPlanElements().get(i - 1);
					Activity nextActivity = (Activity) plan.getPlanElements().get(i + 1);
					Leg leg = (Leg) plan.getPlanElements().get(i);

					if (previousActivity.getType().equals("outside") || nextActivity.getType().equals("outside")) {
						leg.setMode("outside");
						leg.setRoute(null);
					}

					previousActivity.getAttributes().putAttribute("initialModeOfNextTrip", leg.getMode());
				}
			}

			numberOfProcessedPersons++;
			logger.info(String.format("Converted %d/%d persons (%.2f%%)", numberOfProcessedPersons,
					totalNumberOfPersons, 100.0 * numberOfProcessedPersons / totalNumberOfPersons));
		}

		logger.info(String.format("Removed %d/%d persons (%.2f%%)", removedNumberOfPersons, totalNumberOfPersons,
				100.0 * removedNumberOfPersons / totalNumberOfPersons));

		new LongPlanFilter(8, new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE)).run(population);
	}

	private void prepareSchedule(TransitSchedule schedule, Collection<IRIS> iris) {
		// Add IRIS information to transit stops if their link is in AV network
		for (TransitStopFacility facility : schedule.getFacilities().values()) {
			Optional<IRIS> facilityIRIS = findIRIS(facility.getCoord(), iris);

			if (facilityIRIS.isPresent()) {
				facility.getAttributes().putAttribute("iris", facilityIRIS.get().code);
			}
		}
	}

	private void prepareConfig(Config config) {
		config.controler().setLastIteration(100);
		config.controler().setRoutingAlgorithmType(RoutingAlgorithmType.AStarLandmarks);

		config.qsim().setFlowCapFactor(1e6);
		config.qsim().setStorageCapFactor(1e6);

		config.planCalcScore().setMarginalUtlOfWaitingPt_utils_hr(-2.3);
		config.planCalcScore().setUtilityOfLineSwitch(-0.17);
		config.planCalcScore().setPerforming_utils_hr(0.0);
		config.planCalcScore().getOrCreateModeParams("walk").setMarginalUtilityOfTraveling(-6.46);
		config.planCalcScore().getOrCreateModeParams("pt").setMarginalUtilityOfTraveling(-1.32);
		config.planCalcScore().getOrCreateModeParams("access_walk").setMarginalUtilityOfTraveling(-4.0);
		config.planCalcScore().getOrCreateModeParams("egress_walk").setMarginalUtilityOfTraveling(-4.0);
		config.planCalcScore().getOrCreateModeParams("av").setMarginalUtilityOfTraveling(0.0);
		config.planCalcScore().getOrCreateModeParams("outside").setMarginalUtilityOfTraveling(0.0);
		config.planCalcScore().getOrCreateModeParams("av_direct").setMarginalUtilityOfTraveling(0.0);
		config.planCalcScore().getOrCreateModeParams("av_feeder").setMarginalUtilityOfTraveling(0.0);
		config.plansCalcRoute().setNetworkModes(Arrays.asList("car", "car_passenger"));
		config.plansCalcRoute().getModeRoutingParams().get("walk").setBeelineDistanceFactor(1.3);
		config.plansCalcRoute().getModeRoutingParams().get("walk").setTeleportedModeSpeed(1.2);
		config.plansCalcRoute().getModeRoutingParams().get("bike").setBeelineDistanceFactor(1.4);
		config.plansCalcRoute().getModeRoutingParams().get("bike").setTeleportedModeSpeed(3.1);
		config.plansCalcRoute().getOrCreateModeRoutingParams("outside").setBeelineDistanceFactor(1.0);
		config.plansCalcRoute().getOrCreateModeRoutingParams("outside").setTeleportedModeSpeed(1e9);

		ActivityParams activityParams = new ActivityParams("av interaction");
		activityParams.setScoringThisActivityAtAll(false);
		config.planCalcScore().addActivityParams(activityParams);

		activityParams = new ActivityParams("outside");
		activityParams.setScoringThisActivityAtAll(false);
		config.planCalcScore().addActivityParams(activityParams);

		config.strategy().setMaxAgentPlanMemorySize(1);

		StrategySettings strategy = new StrategySettings();
		strategy.setStrategyName("KeepLastSelected");
		strategy.setWeight(0.85);
		config.strategy().addStrategySettings(strategy);

		strategy = new StrategySettings();
		strategy.setStrategyName("custom");
		strategy.setWeight(0.15);
		config.strategy().addStrategySettings(strategy);

		config.transitRouter().setAdditionalTransferTime(0.0);
		config.transitRouter().setDirectWalkFactor(1.0);
		config.transitRouter().setExtensionRadius(200.0);
		config.transitRouter().setMaxBeelineWalkConnectionDistance(100.0);
		config.transitRouter().setSearchRadius(1500.0);

		config.network().setInputFile("paris_network.xml.gz");
		config.facilities().setInputFile("paris_facilities.xml.gz");
		config.plans().setInputFile("paris_population.xml.gz");
		config.households().setInputFile(null);
		config.transit().setTransitScheduleFile("paris_transit_schedule.xml.gz");
		config.transit().setVehiclesFile(null);

		DvrpConfigGroup dvrpConfigGroup = new DvrpConfigGroup();
		config.addModule(dvrpConfigGroup);
		dvrpConfigGroup.setNetworkMode("av");

		AVConfigGroup avConfigGroup = new AVConfigGroup();
		config.addModule(avConfigGroup);

		SwissRailRaptorConfigGroup raptorConfig = new SwissRailRaptorConfigGroup();
		config.addModule(raptorConfig);

		raptorConfig.setUseIntermodalAccessEgress(false);
	}

	private Network prepareNetwork(Network network, Collection<IRIS> iris) {
		int totalNumberOfLinks = network.getLinks().size();
		int processedNumberOfLinks = 0;

		// Add IRIS to all links and add "iris" mode
		for (Link link : network.getLinks().values()) {
			Optional<IRIS> linkIris = findIRIS(link.getCoord(), iris);

			if (linkIris.isPresent()) {
				link.getAttributes().putAttribute("iris", linkIris.get().code);

				if (link.getAllowedModes().contains("car")) {
					Set<String> modes = new HashSet<>(link.getAllowedModes());
					modes.add("iris");
					link.setAllowedModes(modes);
				}
			}

			processedNumberOfLinks++;
			logger.info(String.format("Converted %d/%d links (%.2f%%)", processedNumberOfLinks, totalNumberOfLinks,
					100.0 * processedNumberOfLinks / totalNumberOfLinks));
		}

		// Add "car_passenger" to all "car" links
		for (Link link : network.getLinks().values()) {
			if (link.getAllowedModes().contains("car")) {
				Set<String> modes = new HashSet<>(link.getAllowedModes());
				modes.add("car_passenger");
				link.setAllowedModes(modes);
			}
		}

		// Create av network and clean it (= cleaned IRIS network)
		Network avNetwork = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(network).filter(avNetwork, Collections.singleton("iris"));
		new NetworkCleaner().run(avNetwork);

		// Feed back av mode
		for (Link link : network.getLinks().values()) {
			if (avNetwork.getLinks().containsKey(link.getId())) {
				Set<String> modes = new HashSet<>(link.getAllowedModes());
				modes.add("av");
				link.setAllowedModes(modes);
			}
		}

		return avNetwork;
	}

	private Optional<IRIS> findIRIS(Coord coord, Collection<IRIS> iris) {
		for (IRIS candidate : iris) {
			if (candidate.containsCoordinate(coord)) {
				return Optional.of(candidate);
			}
		}

		return Optional.empty();
	}

	private Map<String, IRIS> readIRIS(File path) throws MalformedURLException, IOException {
		DataStore dataStore = DataStoreFinder.getDataStore(Collections.singletonMap("url", path.toURI().toURL()));
		SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);
		SimpleFeatureCollection featureCollection = featureSource.getFeatures();
		SimpleFeatureIterator featureIterator = featureCollection.features();

		Map<String, IRIS> iris = new HashMap<>();

		while (featureIterator.hasNext()) {
			SimpleFeature feature = featureIterator.next();
			Geometry geometry = (Geometry) feature.getDefaultGeometry();
			String code = (String) feature.getAttribute("CODE_IRIS");
			iris.put(code, new IRIS(geometry, code));
		}

		featureIterator.close();
		dataStore.dispose();

		return iris;
	}

	private void applyOutside(Scenario scenario) {
		Coord outsideCoord = scenario.getNetwork().getLinks().values().iterator().next().getCoord();

		Node outsideNode1 = scenario.getNetwork().getFactory().createNode(Id.createNodeId("outside1"), outsideCoord);
		Node outsideNode2 = scenario.getNetwork().getFactory().createNode(Id.createNodeId("outside2"), outsideCoord);
		Link outsideLink = scenario.getNetwork().getFactory().createLink(Id.createLinkId("outside"), outsideNode1,
				outsideNode2);
		outsideLink.setAllowedModes(Collections.singleton("outside"));

		scenario.getNetwork().addNode(outsideNode1);
		scenario.getNetwork().addNode(outsideNode2);
		scenario.getNetwork().addLink(outsideLink);

		ActivityFacility facility = scenario.getActivityFacilities().getFactory().createActivityFacility(
				Id.create("outside", ActivityFacility.class), outsideCoord, outsideLink.getId());
		scenario.getActivityFacilities().addActivityFacility(facility);
	}

	static public void main(String[] args) throws ConfigurationException, MalformedURLException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("scenario-path", "population-path") //
				.build();

		new MakeScenario().run(new File(cmd.getOptionStrict("scenario-path")),
				new File(cmd.getOptionStrict("population-path")));
	}

}
