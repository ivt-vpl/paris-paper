package ch.ethz.matsim.papers.paris_paper.mode_choice.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraint;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraintFactory;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;

public class ParisModeSelectionConstraint implements TripConstraint {
	private final Collection<String> FIXED_MODES = Arrays.asList("walk", "bike", "outside", "car_passenger");
	private final Collection<String> RESTRICTED_MODES = Arrays.asList("car");

	@Override
	public boolean validateBeforeEstimation(ModeChoiceTrip trip, String modeCandidate, List<String> previousModes) {
		String initialMode = (String) trip.getTripInformation().getOriginActivity().getAttributes()
				.getAttribute("initialModeOfNextTrip");

		if (FIXED_MODES.contains(initialMode)) {
			if (!modeCandidate.equals(initialMode)) {
				return false;
			}
		}

		if (FIXED_MODES.contains(modeCandidate)) {
			if (!modeCandidate.equals(initialMode)) {
				return false;
			}
		}

		if (RESTRICTED_MODES.contains(modeCandidate)) {
			if (!RESTRICTED_MODES.contains(initialMode)) {
				return false;
			}
		}

		return true;
	}

	@Override
	public boolean validateAfterEstimation(ModeChoiceTrip arg0, TripCandidate arg1, List<TripCandidate> arg2) {
		return true;
	}

	static public class Factory implements TripConstraintFactory {
		@Override
		public TripConstraint createConstraint(List<ModeChoiceTrip> arg0, Collection<String> arg1) {
			return new ParisModeSelectionConstraint();
		}
	}
}
