package ch.ethz.matsim.papers.paris_paper.av.pricing;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.OutputDirectoryHierarchy;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av_cost_calculator.CostCalculator;
import ch.ethz.matsim.av_cost_calculator.CostCalculatorExecutor;
import ch.ethz.matsim.av_cost_calculator.run.AVValidator;
import ch.ethz.matsim.av_cost_calculator.run.AnyAVValidator;
import ch.ethz.matsim.av_cost_calculator.run.PricingAnalysisHandler;
import ch.ethz.matsim.av_cost_calculator.run.SingleOccupancyAnalysisHandler;
import ch.ethz.matsim.papers.paris_paper.RunParisPaper;

public class AVPricingModule extends AbstractModule {
	private final double scenarioScale;
	private final int horizon;

	public AVPricingModule(double scenarioScale, int horizon) {
		this.scenarioScale = scenarioScale;
		this.horizon = horizon;
	}

	@Override
	public void install() {
		addEventHandlerBinding().to(PricingAnalysisHandler.class);
		addControlerListenerBinding().to(AVPricingListener.class);
	}

	@Provides
	@Singleton
	public AVPricingListener provideAVPricingListener(PricingAnalysisHandler handler, CostCalculatorExecutor executor,
			OutputDirectoryHierarchy output) throws IOException {
		return new AVPricingListener(scenarioScale, horizon, handler, executor, output);
	}

	@Provides
	@Singleton
	public PricingAnalysisHandler providePricingAnalysisHandler(@Named(AVModule.AV_MODE) Network network) {
		double totalTime = 24.0 * 3600.0;
		AVValidator validator = new AnyAVValidator();
		return new SingleOccupancyAnalysisHandler(network, validator, totalTime);
	}

	@Provides
	@Singleton
	public CostCalculatorExecutor provideCostCalculatorExecutor(OutputDirectoryHierarchy outputHierarchy) {
		URL sourceURL = CostCalculator.class.getClassLoader().getResource("ch/ethz/matsim/av_cost_calculator/");
		URL templateURL = RunParisPaper.class.getClassLoader()
				.getResource("ch/ethz/matsim/papers/paris_paper/InputBerlin.xlsx");

		File workingDirectory = new File(outputHierarchy.getTempPath() + "/av_cost_calculator");
		workingDirectory.mkdirs();

		return new CostCalculatorExecutor(workingDirectory, sourceURL, templateURL);
	}
}
