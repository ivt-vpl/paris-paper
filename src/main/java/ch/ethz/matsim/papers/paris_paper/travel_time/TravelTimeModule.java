package ch.ethz.matsim.papers.paris_paper.travel_time;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.mobsim.qsim.qnetsimengine.ConfigurableQNetworkFactory;
import org.matsim.core.mobsim.qsim.qnetsimengine.QNetworkFactory;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutilityFactory;
import org.matsim.core.router.util.TravelTime;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.framework.AVModule;

public class TravelTimeModule extends AbstractModule {
	private final TravelTime travelTime;

	public TravelTimeModule(TravelTime travelTime) {
		this.travelTime = travelTime;
	}

	@Override
	public void install() {
		addTravelTimeBinding(TransportMode.car).toInstance(travelTime);
		addTravelTimeBinding("car_passenger").toInstance(travelTime);
		addTravelTimeBinding(AVModule.AV_MODE).toInstance(travelTime);
		addTravelDisutilityFactoryBinding(TransportMode.car).toInstance(new OnlyTimeDependentTravelDisutilityFactory());
		addTravelDisutilityFactoryBinding("car_passenger").toInstance(new OnlyTimeDependentTravelDisutilityFactory());
		bind(QNetworkFactory.class).to(ConfigurableQNetworkFactory.class);
		addTravelTimeBinding(AVModule.AV_MODE).toInstance(travelTime);
	}

	@Provides
	@Singleton
	public TravelTimeLinkSpeedCalculator provideTravelTimeLinkSpeedCalculator(@Named("car") TravelTime travelTime) {
		return new TravelTimeLinkSpeedCalculator(travelTime);
	}

	@Provides
	@Singleton
	public ConfigurableQNetworkFactory provideConfigurableQNetworkFactory(EventsManager eventsManager,
			Scenario scenario, TravelTimeLinkSpeedCalculator linkSpeedCalculator) {
		ConfigurableQNetworkFactory factory = new ConfigurableQNetworkFactory(eventsManager, scenario);
		factory.setLinkSpeedCalculator(linkSpeedCalculator);
		return factory;
	}

}
