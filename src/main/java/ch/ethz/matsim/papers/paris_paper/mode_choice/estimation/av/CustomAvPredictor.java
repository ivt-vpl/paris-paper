package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.av;

import java.util.List;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.TripStructureUtils.Trip;
import org.matsim.facilities.ActivityFacilities;

import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.papers.paris_paper.av.ParisAVRoute;

public class CustomAvPredictor {
	final private TripRouter router;
	final private Network network;

	public CustomAvPredictor(TripRouter router, ActivityFacilities facilities, Network network) {
		this.router = router;
		this.network = network;
	}

	public CustomAvPrediction predict(ModeChoiceTrip trip) {
		Trip tripInformation = trip.getTripInformation();

		LinkWrapperFacility originFacility = new LinkWrapperFacility(
				this.network.getLinks().get(tripInformation.getOriginActivity().getLinkId()));
		LinkWrapperFacility destinationFacility = new LinkWrapperFacility(
				this.network.getLinks().get(tripInformation.getDestinationActivity().getLinkId()));

		List<? extends PlanElement> result = router.calcRoute("av", originFacility, destinationFacility,
				tripInformation.getOriginActivity().getEndTime(), trip.getPerson());

		ParisAVRoute route = (ParisAVRoute) ((Leg) result.get(0)).getRoute();
		return new CustomAvPrediction(route.getDistance(), route.getTravelTime(), route.getWaitingTime());
	}
}
