package ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.av;

public class CustomAvPrediction {
	final public double distance;
	final public double inVehicleTime;
	final public double waitingTime;

	public CustomAvPrediction(double distance, double inVehicleTime, double waitingTime) {
		this.distance = distance;
		this.inVehicleTime = inVehicleTime;
		this.waitingTime = waitingTime;
	}
}
