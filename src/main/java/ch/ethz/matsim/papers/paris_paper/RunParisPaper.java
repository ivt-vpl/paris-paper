package ch.ethz.matsim.papers.paris_paper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.matsim.api.core.v01.Scenario;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;

import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.papers.paris_paper.av.ParisAVModule;
import ch.ethz.matsim.papers.paris_paper.av.ParisQSimModule;
import ch.ethz.matsim.papers.paris_paper.av.pricing.AVPricingModule;
import ch.ethz.matsim.papers.paris_paper.mode_choice.ParisModeChoiceModule;
import ch.ethz.matsim.papers.paris_paper.travel_time.SerializableTravelTime;
import ch.ethz.matsim.papers.paris_paper.travel_time.TravelTimeModule;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorModule;

public class RunParisPaper {
	static public void main(String[] args) throws ConfigurationException, FileNotFoundException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.allowPositionalArguments(false) //
				.requireOptions("config-path", "scenario-scale") //
				.allowOptions("travel-time-path", "fleet-size", "scoring-path", "model-type", "use-dynamic-price") //
				.allowPrefixes("scoring") //
				.build();

		// Load config
		Config config = ConfigUtils.loadConfig(cmd.getOptionStrict("config-path"), new DvrpConfigGroup(),
				new AVConfigGroup(), new SwissRailRaptorConfigGroup());
		cmd.applyConfiguration(config);

		// Load scenario
		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		// Load travel time
		TravelTime travelTime = new FreeSpeedTravelTime();

		if (cmd.hasOption("travel-time-path")) {
			InputStream inputStream = new GZIPInputStream(
					new FileInputStream(new File(cmd.getOptionStrict("travel-time-path"))));
			travelTime = SerializableTravelTime.readBinary(inputStream);
			inputStream.close();
		}

		long fleetSize = cmd.getOption("fleet-size").map(Integer::parseInt).orElse(0);
		double constantWaitingTime = cmd.getOption("constant-waiting-time").map(Double::parseDouble).orElse(Double.NaN);
		double scenarioScale = Double.parseDouble(cmd.getOptionStrict("scenario-scale"));

		Controler controler = new Controler(scenario);
		controler.addOverridingModule(new DvrpTravelTimeModule());
		controler.addOverridingModule(new ParisModeChoiceModule(cmd));
		controler.addOverridingModule(new SwissRailRaptorModule());
		controler.addOverridingModule(new BaselineTransitModule());
		controler.addOverridingModule(new AVModule());
		controler.addOverridingModule(new ParisAVModule(fleetSize, constantWaitingTime));
		controler.addOverridingModule(new TravelTimeModule(travelTime));
		controler.addOverridingModule(new ParisQSimModule());
		controler.addOverridingModule(new AVPricingModule(scenarioScale, 10));

		controler.run();
	}
}
