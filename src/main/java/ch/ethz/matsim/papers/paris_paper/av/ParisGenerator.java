package ch.ethz.matsim.papers.paris_paper.av;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.contrib.dvrp.data.Vehicle;
import org.matsim.core.gbl.MatsimRandom;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.config.AVGeneratorConfig;
import ch.ethz.matsim.av.data.AVVehicle;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.generator.AVGenerator;

public class ParisGenerator implements AVGenerator {
	private final List<Link> links;
	private final List<Double> cdf;
	private final int fleetSize;
	private final Random random;
	private int count = 0;

	public ParisGenerator(List<Link> links, List<Double> cdf, int fleetSize, Random random) {
		this.links = links;
		this.cdf = cdf;
		this.fleetSize = fleetSize;
		this.random = random;
	}

	@Override
	public boolean hasNext() {
		return count < fleetSize;
	}

	@Override
	public AVVehicle next() {
		count++;

		double selector = random.nextDouble();
		int selectorIndex = 0;

		while (cdf.get(selectorIndex) < selector) {
			selectorIndex++;
		}

		Id<Vehicle> vehicleId = Id.create(String.format("av_%d", count), Vehicle.class);

		Link startLink = links.get(selectorIndex);

		return new AVVehicle(vehicleId, startLink, 1.0, 0.0, Double.POSITIVE_INFINITY);
	}

	@Singleton
	public static class Factory implements AVGeneratorFactory {
		private final List<Link> links;
		private final List<Double> cdf;

		@Inject
		public Factory(Population population, @Named(AVModule.AV_MODE) Network network) {
			links = network.getLinks().values().stream().filter(l -> l.getAttributes().getAttribute("iris") != null)
					.collect(Collectors.toList());

			List<Double> weights = new ArrayList<>(Collections.nCopies(links.size(), 0.0));
			List<Id<Link>> linkIds = links.stream().map(Link::getId).collect(Collectors.toList());

			for (Person person : population.getPersons().values()) {
				Plan plan = person.getSelectedPlan();

				if (plan.getPlanElements().size() > 0) {
					Activity firstActivity = (Activity) plan.getPlanElements().get(0);
					int linkIndex = linkIds.indexOf(firstActivity.getLinkId());

					if (linkIndex > -1) {
						weights.set(linkIndex, weights.get(linkIndex) + 1);
					}
				}
			}

			cdf = new ArrayList<>(links.size());

			for (int i = 0; i < links.size(); i++) {
				if (i == 0) {
					cdf.add(weights.get(0));
				} else {
					cdf.add(cdf.get(i - 1) + weights.get(i));
				}
			}

			for (int i = 0; i < links.size(); i++) {
				cdf.set(i, cdf.get(i) / cdf.get(links.size() - 1));
			}
		}

		@Override
		public AVGenerator createGenerator(AVGeneratorConfig generatorConfig) {
			Random random = MatsimRandom.getLocalInstance();
			return new ParisGenerator(links, cdf, (int) generatorConfig.getNumberOfVehicles(), random);
		}

	}
}
