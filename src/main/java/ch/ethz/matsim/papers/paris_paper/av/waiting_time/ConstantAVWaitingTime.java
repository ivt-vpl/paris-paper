package ch.ethz.matsim.papers.paris_paper.av.waiting_time;

import org.matsim.facilities.Facility;

public class ConstantAVWaitingTime implements AVWaitingTime {
	private final double waitingTime;

	public ConstantAVWaitingTime(double waitingTime) {
		this.waitingTime = waitingTime;
	}

	@Override
	public double getWaitingTime(Facility<?> facility, double time) {
		return waitingTime;
	}
}
