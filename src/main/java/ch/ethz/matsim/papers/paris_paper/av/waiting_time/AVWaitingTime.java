package ch.ethz.matsim.papers.paris_paper.av.waiting_time;

import org.matsim.facilities.Facility;

public interface AVWaitingTime {
	double getWaitingTime(Facility<?> facility, double time);
}
