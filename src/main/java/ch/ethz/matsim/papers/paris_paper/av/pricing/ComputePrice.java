package ch.ethz.matsim.papers.paris_paper.av.pricing;

import java.util.Map;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;

import ch.ethz.matsim.av_cost_calculator.run.AVValidator;
import ch.ethz.matsim.av_cost_calculator.run.AnyAVValidator;
import ch.ethz.matsim.av_cost_calculator.run.ParameterBuilder;
import ch.ethz.matsim.av_cost_calculator.run.SingleOccupancyAnalysisHandler;

public class ComputePrice {
	static public void main(String[] args) {
		String networkPath = args[0];
		String eventsPath = args[1];

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(networkPath);

		double totalTime = 24.0 * 3600.0;

		AVValidator validator = new AnyAVValidator();
		SingleOccupancyAnalysisHandler handler = new SingleOccupancyAnalysisHandler(network, validator, totalTime);

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(handler);

		new MatsimEventsReader(eventsManager).readFile(eventsPath);

		System.out.println(handler.getRelativeActiveTime());
		System.out.println(handler.getRelativeEmptyDistance());

		Map<String, String> parameters = new ParameterBuilder(0.1, 24.0 * 3600.0, "Solo_Berlin").build(handler);

		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}
}
