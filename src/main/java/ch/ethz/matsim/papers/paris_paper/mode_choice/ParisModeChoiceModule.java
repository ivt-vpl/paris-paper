package ch.ethz.matsim.papers.paris_paper.mode_choice;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.config.groups.PlansCalcRouteConfigGroup;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutilityFactory;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;
import org.matsim.facilities.ActivityFacilities;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.mode_choice.constraints.CompositeTripConstraintFactory;
import ch.ethz.matsim.mode_choice.estimation.ModeAwareTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TripEstimatorCache;
import ch.ethz.matsim.mode_choice.framework.ModeAvailability;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceModel;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceModel.FallbackBehaviour;
import ch.ethz.matsim.mode_choice.framework.trip_based.TripBasedModel;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;
import ch.ethz.matsim.mode_choice.framework.utilities.MaximumUtilitySelector;
import ch.ethz.matsim.mode_choice.framework.utilities.MultinomialSelector;
import ch.ethz.matsim.mode_choice.framework.utilities.UtilitySelectorFactory;
import ch.ethz.matsim.mode_choice.prediction.TeleportationPredictor;
import ch.ethz.matsim.mode_choice.replanning.ModeChoiceModelStrategy;
import ch.ethz.matsim.mode_choice.replanning.NonSelectedPlanSelector;
import ch.ethz.matsim.papers.paris_paper.av.pricing.AVPricingListener;
import ch.ethz.matsim.papers.paris_paper.mode_choice.constraints.ParisModeSelectionConstraint;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.CustomModeChoiceParameters;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.av.CustomAvEstimator;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.av.CustomAvPredictor;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.car.CustomCarEstimator;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.car.CustomCarPredictor;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.other.CustomBikeEstimator;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.other.CustomCarPassengerEstimator;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.other.CustomOutsideEstimator;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.other.CustomWalkEstimator;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.pt.CustomPublicTransportEstimator;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.pt.CustomPublicTransportPredictor;
import ch.ethz.matsim.papers.paris_paper.mode_choice.estimation.pt.subscription.SubscriptionFinder;

public class ParisModeChoiceModule extends AbstractModule {
	private static final List<String> MODES = Arrays.asList("car", "pt", "bike", "walk", "av", "outside",
			"car_passenger");
	private static final List<String> CACHED_MODES = Arrays.asList("pt", "bike", "walk", "av");

	private final CommandLine cmd;

	public ParisModeChoiceModule(CommandLine cmd) {
		this.cmd = cmd;
	}

	@Override
	public void install() {
		addPlanStrategyBinding("custom").toProvider(ModeChoiceModelStrategy.class);
		bindPlanSelectorForRemoval().to(NonSelectedPlanSelector.class);

		addTravelTimeBinding("car").toInstance(new FreeSpeedTravelTime());
		addTravelDisutilityFactoryBinding("car").toInstance(new OnlyTimeDependentTravelDisutilityFactory());
	}

	@Provides
	@Singleton
	public CustomModeChoiceParameters provideCustomModeChoiceParameters()
			throws ConfigurationException, JsonParseException, JsonMappingException, IOException {
		CustomModeChoiceParameters parameters = new CustomModeChoiceParameters();

		if (cmd.hasOption("scoring-path")) {
			File inputPath = new File(cmd.getOptionStrict("scoring-path"));

			if (inputPath.exists()) {
				parameters = new ObjectMapper().readValue(new File(cmd.getOptionStrict("scoring-path")),
						CustomModeChoiceParameters.class);
			} else {
				File outputPath = new File(inputPath.toString() + "_");
				new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).writeValue(outputPath,
						new CustomModeChoiceParameters());
				throw new IllegalStateException("Wrote template scoring file to " + outputPath.toString());
			}
		}

		String prefix = "scoring:";

		List<String> scoringOptions = cmd.getAvailableOptions().stream().filter(o -> o.startsWith(prefix))
				.collect(Collectors.toList());

		Map<String, String> rawParameters = new HashMap<>();

		for (String option : scoringOptions) {
			rawParameters.put(option.substring(prefix.length()), cmd.getOptionStrict(option));
		}

		parameters.load(rawParameters);

		return parameters;
	}

	@Provides
	@Singleton
	public SubscriptionFinder provideSubscriptionFinder(Population population) {
		return new SubscriptionFinder();
	}

	@Provides
	public ModeChoiceModel provideModeChoiceModel(PlansCalcRouteConfigGroup routeConfig,
			CustomModeChoiceParameters parameters, TripRouter router, SubscriptionFinder subscriptionFinder,
			Network network, ActivityFacilities facilities, AVPricingListener priceListener) throws ConfigurationException {
		ModeAvailability modeAvailability = new CarModeAvailability(MODES);

		// ESTIMATORS
		CustomOutsideEstimator outsideEstimator = new CustomOutsideEstimator();
		CustomCarPassengerEstimator carPassengerEstimator = new CustomCarPassengerEstimator();

		double crowflyDistanceFactorWalk = routeConfig.getModeRoutingParams().get("walk").getBeelineDistanceFactor();
		double speedWalk = routeConfig.getModeRoutingParams().get("walk").getTeleportedModeSpeed();

		TeleportationPredictor teleportationPredictorWalk = new TeleportationPredictor(crowflyDistanceFactorWalk,
				speedWalk);
		CustomWalkEstimator walkEstimator = new CustomWalkEstimator(parameters, teleportationPredictorWalk);

		double crowflyDistanceFactorBike = routeConfig.getModeRoutingParams().get("bike").getBeelineDistanceFactor();
		double speedBike = routeConfig.getModeRoutingParams().get("bike").getTeleportedModeSpeed();

		TeleportationPredictor teleportationPredictorBike = new TeleportationPredictor(crowflyDistanceFactorBike,
				speedBike);
		CustomBikeEstimator bikeEstimator = new CustomBikeEstimator(parameters, teleportationPredictorBike);

		CustomPublicTransportPredictor publicTransportPredictor = new CustomPublicTransportPredictor(router, facilities,
				network);
		CustomPublicTransportEstimator publicTransportEstimator = new CustomPublicTransportEstimator(parameters,
				publicTransportPredictor, subscriptionFinder);

		CustomCarPredictor carPredictor = new CustomCarPredictor(router, facilities, network);
		CustomCarEstimator carEstimator = new CustomCarEstimator(parameters, carPredictor);
		
		boolean useDynamicPrice = Boolean.parseBoolean(cmd.getOption("use-dynamic-price").orElse("false"));

		CustomAvPredictor avPredictor = new CustomAvPredictor(router, facilities, network);
		CustomAvEstimator avEstimator = new CustomAvEstimator(parameters, avPredictor, priceListener, useDynamicPrice);

		ModeAwareTripEstimator modeAwareEstimator = new ModeAwareTripEstimator();
		modeAwareEstimator.addEstimator("car", carEstimator);
		modeAwareEstimator.addEstimator("walk", walkEstimator);
		modeAwareEstimator.addEstimator("bike", bikeEstimator);
		modeAwareEstimator.addEstimator("pt", publicTransportEstimator);
		modeAwareEstimator.addEstimator("av", avEstimator);
		modeAwareEstimator.addEstimator("outside", outsideEstimator);
		modeAwareEstimator.addEstimator("car_passenger", carPassengerEstimator);

		TripEstimatorCache estimator = new TripEstimatorCache(modeAwareEstimator, CACHED_MODES);

		// CONSTRAINTS

		CompositeTripConstraintFactory tripConstraintFactory = new CompositeTripConstraintFactory();
		tripConstraintFactory.addFactory(new ParisModeSelectionConstraint.Factory());

		UtilitySelectorFactory<TripCandidate> tripSelectorFactory;

		switch (cmd.getOption("model-type").orElse("multinomial")) {
		case "multinomial":
			tripSelectorFactory = new MultinomialSelector.Factory<>(700.0);
			break;
		case "best-response":
			tripSelectorFactory = new MaximumUtilitySelector.Factory<>();
			break;
		default:
			throw new IllegalStateException();
		}

		return new TripBasedModel(estimator, modeAvailability, tripConstraintFactory, tripSelectorFactory,
				FallbackBehaviour.INITIAL_CHOICE);
	}

}
