package ch.ethz.matsim.papers.paris_paper.av.waiting_time.estimation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.PersonEntersVehicleEvent;
import org.matsim.api.core.v01.events.handler.PersonDepartureEventHandler;
import org.matsim.api.core.v01.events.handler.PersonEntersVehicleEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.facilities.Facility;

import ch.ethz.matsim.papers.paris_paper.av.waiting_time.AVWaitingTime;

public class MovingAverageWaitingTime
		implements AVWaitingTime, PersonDepartureEventHandler, PersonEntersVehicleEventHandler {
	private final double estimationStartTime;
	private final double estimationEndTime;
	private final double estimationInterval;
	private final int numberOfEstimationBins;

	private final Map<Id<Link>, Integer> irisIndices;
	private final IndexEstimator estimator;

	private final double stuckWaitingTime;

	public MovingAverageWaitingTime(double estimationStartTime, double estimationEndTime, double estimationInterval,
			int horizon, double defaultWaitingTime, double stuckWaitingTime, Network network) {
		this.estimationStartTime = estimationStartTime;
		this.estimationEndTime = estimationEndTime;
		this.estimationInterval = estimationInterval;
		this.numberOfEstimationBins = (int) Math.ceil((estimationEndTime - estimationStartTime) / estimationInterval);
		this.irisIndices = createIRISIndices(network);
		this.stuckWaitingTime = stuckWaitingTime;

		this.estimator = new MovingAverageIndexEstimator(irisIndices.size(), numberOfEstimationBins, horizon,
				defaultWaitingTime);
	}

	private Map<Id<Link>, Integer> createIRISIndices(Network network) {
		List<String> names = new LinkedList<>();
		List<Integer> counts = new LinkedList<>();

		Map<Id<Link>, Integer> indices = new HashMap<>();

		for (Link link : network.getLinks().values()) {
			String iris = (String) link.getAttributes().getAttribute("iris");

			if (iris == null) {
				iris = "__outside__";
			}

			if (!names.contains(iris)) {
				names.add(iris);
				counts.add(0);
			}

			int index = names.indexOf(iris);
			indices.put(link.getId(), index);
			counts.set(index, counts.get(index) + 1);
		}

		return indices;
	}

	@Override
	public double getWaitingTime(Facility<?> facility, double time) {
		return Math.max(0, estimator.estimate(getIRISIndex(facility.getLinkId()), getTimeIndex(time)));
	}

	private int getIRISIndex(Id<Link> linkId) {
		return irisIndices.get(linkId);
	}

	private int getTimeIndex(double time) {
		if (time < estimationStartTime) {
			return 0;
		} else if (time >= estimationEndTime) {
			return numberOfEstimationBins - 1;
		} else {
			return (int) Math.floor((time - estimationStartTime) / estimationInterval);
		}
	}

	private final Map<Id<Person>, PersonDepartureEvent> departureEvents = new HashMap<>();

	private void recordPickupTime(double pickupTime, double time, Id<Link> linkId) {
		estimator.record(getIRISIndex(linkId), getTimeIndex(time), pickupTime);
	}

	@Override
	public void handleEvent(PersonDepartureEvent event) {
		if (event.getLegMode().equals("av")) {
			departureEvents.put(event.getPersonId(), event);
		}
	}

	@Override
	public void handleEvent(PersonEntersVehicleEvent event) {
		PersonDepartureEvent departureEvent = departureEvents.remove(event.getPersonId());

		if (departureEvent != null) {
			recordPickupTime(event.getTime() - departureEvent.getTime(), departureEvent.getTime(),
					departureEvent.getLinkId());
		}
	}

	@Override
	public void reset(int iteration) {
		for (PersonDepartureEvent departureEvent : departureEvents.values()) {
			recordPickupTime(stuckWaitingTime, departureEvent.getTime(), departureEvent.getLinkId());
		}

		departureEvents.clear();
		estimator.finish();
	}
}
