package ch.ethz.matsim.papers.paris_paper.av.waiting_time.estimation;

public interface IndexEstimator {
	void record(int itemIndex, int timeIndex, double value);

	double estimate(int itemIndex, int timeIndex);

	void finish();
}
