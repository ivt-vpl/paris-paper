package ch.ethz.matsim.papers.paris_paper.travel_time;

import org.matsim.api.core.v01.network.Link;
import org.matsim.core.mobsim.qsim.qnetsimengine.QVehicle;
import org.matsim.core.mobsim.qsim.qnetsimengine.linkspeedcalculator.LinkSpeedCalculator;
import org.matsim.core.router.util.TravelTime;

public class TravelTimeLinkSpeedCalculator implements LinkSpeedCalculator {
	private final TravelTime travelTime;

	public TravelTimeLinkSpeedCalculator(TravelTime travelTime) {
		this.travelTime = travelTime;
	}

	@Override
	public double getMaximumVelocity(QVehicle vehicle, Link link, double time) {
		return travelTime.getLinkTravelTime(link, time, null, null);
	}
}
