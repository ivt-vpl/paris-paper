\section{Background}

Control and management of automated vehicle fleets has become an active field of
research over the past five years. The following paragraphs shall give a brief
overview of past simulation studies that have been performed for various use
cases around the world.

One of the first large-scale simulations
was performed in \cite{Spieser2014} for Singapore. The study finds that the
whole transport demand of the city could be covered by one third of today's
vehicle fleet if it entirely would consist of automated single-occupancy vehicles.

Subsequently, a series of studies have been performed for the case of Austin, Texas. In \cite{Kara2014}
a grid-based simulation for the city is introduced. For an artificial demand based
on real-world trip generation rates and randomly assigned destinations it is found
that Austin's demand in private car trips could be served by an automated vehicle
fleet that is reduced by 90\% compared to today. The use case is further extended
in \cite{Chen2016}, where an electric charging infrastructure is assumed. Further
studies introduce a more detailed demand for the scenario, based on static trips from the regional household
travel survey (HTS). \cite{Levin2017} introduces congestion to the simulation and
finds that this has a strong impact on fleet size. In \cite{Liu2017} a choice model is applied, though in a post-processing step. After a detailed
daily travel demand from HTS is simulated using an approach that had been applied
to the canton of Zurich before \cite{Boesch2016},
a discrete choice model is fed with information about travel and waiting times
to analyze potential mode shares in Austin. The utility function is entirely based on literature,
no model is estimated from stated preference (SP) data. Finally,
\cite{Fagnant2018} extends the Austin case with a ride-sharing component and finds
that it could reduce wait times for the customers and mitigate the increase of
VKT (vehicle kilometers travelled) due to empty rides. However, customer preferences
for the service are not taken into account.

For the case of Berlin, \cite{heuristic} use a static travel demand
from the regional HTS to create a MATSim \cite{horni2016multi} (see below) simulation with automated
taxis. All car trips within the city boundaries are replaced by the service,
leading to a scenario where one tenth of all vehicles could be replaced if every
agent in the simulation was to use the service with acceptable wait times. In the
same scenario \cite{MaciejewskiBischoff2016b} find that also allowing public transport
users leads to a linear increase in needed fleet size. Finally, \cite{MaciejewskiBischoff2017}
introduce congestion to the simulation showing that without significant gains in
road capacity due to automation a fleet of automated taxis serving all of the city's
demand would worsen congestion dramatically. On the other hand, automated vehicles
are found to be likely to mitigate parking search problems in the city \cite{BischoffMaciejewski2018}.

For Zurich, \cite{amod_zh} shows that under ideal flow conditions a fleet size
of around 7000 to 14\thinspace000 automated taxis would be able to serve the mobility demand
of the city. Using a detailed agent-based daily travel demand from HTS data, it is shown
that the dispatching strategy has a large impact on the performance of the fleet.
Extended work \cite{astra_report} combines this simulation with a detailed model
of costs for automated mobility \cite{costpaper}. A choice model for conventional
and automated modes of transport alike is estimated from a large-scale
stated preference survey in the canton of Zurich and added dynamically to the simulation. The study constitutes the first
simulation in which a closed cycle between simulation of demand and supply is able
to not only estimate what fleet size would be able to serve a certain demand, but
also for which fleet size and service characteristics customers would be willing
to pay.

A similar approach is shown in \cite{Wen2018} for a not further specified
European city. Not using an agent-based transport model, the simulation
is based on trip generation rates, which are in turn dependent on a discrete
choice model that is fed with travel and waiting times from a previous simulation
of the generated trips. Hence, equilibrium of supply and demand
can be achieved.

Further simulations based on Aimsum have been performed for Munich \cite{Dandl1, Dandl2} where HTS
data and real-world car-sharing data is used to establish a demand that is to
be covered by automated taxis. \cite{Martinez2017} introduce another simulation
platform for Lisbon, where HTS data is used in a static way, but where the choice
for various automated mobility services is determined by a heuristic model based on expert
opinion.

For France, a number of studies can be mentioned. \cite{Berrada1} propose a simulation
framework of automated busses in Palaiseau close to Paris. Demand is static and based on
trip-generation rates at well-defined spots in the system. The simulation is extended
in \cite{Berrada2}, where a choice model is introduced to establish a dynamic demand.
Utilities for the choice model are based on literature values.

A first simulation with automated vehicles in Paris is presented in \cite{Vosooghi1}.
Based on census and HTS data, similar to the paper at hand, the characteristics
of a static fleet of 17\thinspace000 vehicles are examined serving a detailed dynamic
demand based on a classic MATSim simulation. No SP data is used, but a heuristic
that, based on literature, models the attractivity of an automated taxi service
for different sociodemographic and income-based groups. As a first study in the
field the impact of such a fleet on sociodemographic groups is studied in detail.
The framework is further applied to the city of Rouen \cite{Vosooghi2}, where a
fleet sizing given diverse user group preferences is performed. It is found that
user preferences may have a strong impact on the utilization of such a service.
