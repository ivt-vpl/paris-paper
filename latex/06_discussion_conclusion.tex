\section{Discussion}

While the results presented above are able to provide a first idea of how a fleet of
automated taxis in Paris could look like, they are by no means definitive. Three
important components of the simulation can be improved easily, given the
necessary data becomes available:

\begin{itemize}
\item First, the cost model of automated mobility for the case of Berlin
is used in this research as it seems to be closest to Paris among those
cities that have been assessed to far. Applying the methodology of \cite{costpaper}
explicitly for the case of Paris will yield a more consistent picture of
customer prices.
\item Second, the choice model and its parameters are derived from a model
for Zurich due to the lack of a discrete choice model that is compatible
with the choice dimensions provided by MATSim and \^Ile-de-France data. A specifically
estimated choice model for the region or a spatially detailed version of the
\textit{Enqu\^ete globale de transport} to conduct such an estimation would help improve the consistency of the
behavioural component of our model.
\item Third, the ideal case would be a discrete choice model including AMoD services
and the attitudes of the \^Ile-de-France population towards such an offer.
\end{itemize}

For the sake of brevity, the analysis in this paper is restricted to the
interplay of (revenue neutral) prices and the AMoD demand. Of course, our
simulation framework allows the analysis of a multitude of additional dimensions
such as vehicle replacement rates, empty distances, overall driven distance,
emissions, gains and losses in travel time, even gains and losses in (utility-based)
welfare on a per-agent basis. This also makes it possible to analyse the
impact of an AMoD service for specific sociodemographic groups. Especially the
analysis of mode shifts will become important, since a large share of demand in
the presented simulations is generated through former public transport users.

The framework allows to introduce a manifold of additional scenarios
and to answer respective questions:

\begin{itemize}
\item The financial aspects of an AMoD service can be examined. How do specific
profit rates or subsidies influence the system?
\item Instead of door-to-door travels the AMoD fleet can be simulated as
feeder service for the commuter rail and metro network \cite{haslebacher_ma}. How would this influence
the use of bus services in the city? Would this have impact on the elderly
or physically disabled who use such services to avoid inaccessible metro stations?
\item Currently the framework is being extended
to allow for limited pick-up and drop-off spaces in the network as well as
at public transit stations. Will spatial constraints become an issue for
the feasibility of a large-scale AMoD service?
\item More intelligent dispatching (and redistribution) algorithms can be tested.
Looking at previous studies the authors expect a strong effect on the dynamic
demand depending on whether an empty-distance-minimizing dispatcher is used
or one that tries to distribute waiting times equally in the operating area.
While comparative studies in static demand cases have been conducted \cite{amod_zh}, there is no study
to the knowledge of the authors that has assessed different fleet operational strategies
under the presence of customer behaviour.
\item Different scenario diameters can be defined: Currently, the highway
ring of Paris serves as an (computationally feasible) example, which
lacks political as well as land-use considerations. For instance, neither the
main business district La D\'efense is included in the scenario,
nor are the two main airports Charles-de-Gaulle and Orly. Many outer
neighborhoods of the city are neglected, where, for instance, feeder services
to the RER commuter rail could be of benefit to the local population. A further interesting
future path to explore lies therefore in questions of land use policy and
urban development. For both fields the model at hand could yield valuable
inputs.
\end{itemize}

\section{Conclusion}

To conclude, we want to focus on two main insights. First, we show that
taking into account traveller behaviour in demand estimation for an AMoD
service makes a big difference in the results. The dynamic behaviourally
consistent demand is only half the maximal possible
number of trips.

Second, we conclude that given our assumptions an AMoD fleet could operate
within the highway ring of Paris with around 25k vehicles at a price of
0.27 EUR/km, which is low compared to the full cost of using one's own
private vehicle in Paris. This result confirms the findings of previous studies
that an AMoD fleet is indeed an economical alternative to today's private car
ownership.

Our discussion provides an incomplete list of potential future
studies that could be conducted using the framework at hand. While many
of the assumptions in the model to date are best guess, there lies great
potential in improving it through the various ways described above.
