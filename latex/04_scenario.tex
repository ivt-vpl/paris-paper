\section{Synthetic population of Paris}

To perform the proposed fleet sizing experiments, the simulation framework needs
to operate on a population of synthetic agents. The following section
details how such a population is set up for the \^Ile-de-France region, which
surrounds the city of Paris. Afterwards, further modifications for future
scenarios of automated mobility are described.

\subsection{Baseline Scenario}
The novel synthetic agent population of Paris consists of individual persons, aggregated to households,
which have a rich set of individual daily activity schedules. It is known where
people live, where they work, where they perform leisure or shopping
activities, and when and in which order they want to perform them.

The population is created in a multi-step process:

\begin{itemize}
\item For France, a large sample of census data\footnote{https://www.insee.fr/fr/statistiques/3625223?sommaire=3558417} is available for 2015. In the
first synthesis step, the households contained in this data set are filtered such
that only residents of the \^Ile-de-France region remain. Using the provided weights,
a full population for the region is sampled, which includes all relevant socio-demographic
attributes such as age, gender, employment and more.
\item In order to synthesize daily activity chains for those agents, the regional
Household Travel Survey is used (\textit{Enqu\^ete globale de transport}\footnote{http://www.driea.ile-de-france.developpement-durable.gouv.fr/enquete-globale-de-transport-r18.html}). It contains the activity chains
of 35,175 respondents including information about their sociodemographics.
Using these attributes, activity chains are attached to all synthetic agents by
statistical matching.
\item Home locations of all households are known from the census data, but only
on the basis of IRIS zones (see Figure \ref{fig:map}). Therefore, a
discrete location within the respective zone is sampled for each household. For
primary activities (like \textit{work} and \textit{education}) commuter matrices\footnote{https://www.insee.fr/fr/statistiques/3566008?sommaire=3558417}
are used to sample destination zones proportionately given the homes
of the agents. For all other activities (\textit{shopping}, \textit{leisure},
\textit{errand}) locations are sampled at random from the French Enterprise
Census 2016 (\textit{Base permanente des \'equipements}\footnote{https://www.data.gouv.fr/fr/datasets/base-permanente-des-equipements-1/}) such that they are consistent with modes and travel times
in each agent's daily plan.
\item Finally, the synthetic population is converted to the MATSim data format
to be used by the simulation.
\end{itemize}

To model the supply side two major data sources
are used. First, OpenStreetMap data\footnote{https://download.geofabrik.de/europe/france/ile-de-france.html}
is used to create a road network of \^Ile-de-France. Second, public transit schedules,
which are published on a regular basis for the whole region\footnote{https://opendata.stif.info/explore/dataset/offre-horaires-tc-gtfs-idf/information/},
are integrated. These steps are performed using existing tools \cite{poletti} in the MATSim ecosystem.
It is worth noting that all used data sets, except the HTS, are publicly available.

\begin{figure}
\centering
\includegraphics[width=3.5in]{images/map.pdf}
\caption{Map of Paris with the IRIS zoning system. The colored area is used as the operating area in this study. (Map: OpenStreetMap / Wikimedia)}
\label{fig:map}
\end{figure}

Subsequently, the population is simulated in MATSim in combination with the
discrete choice extension. The choice model formulation
is documented in Equations \ref{eq:mode_choice_model_car} to \ref{eq:mode_choice_model_walking}
with the parameter values given in Table \ref{tab:scoring}. Expressions denoted as
$x$ describe choice variables while $\theta$ and $a$ denote calibration parameters
and agent-specific constants, respectively.

\begin{table}[t]
		\caption{Parameters of the discrete mode choice model}
	\label{tab:scoring}
	\centering
	\begin{tabular}{l|lrl}
		\hline\hline
		Car & $\alpha_{\text{car}}$ & $1.35$ & $ $ \\
		& $\beta_{\text{travelTime,car}}$ & $-0.0667$ & $[\text{min}^{-1}]$ \\
		\hline
		Public Transport & $\alpha_{\text{pt}}$ & $0.0$ & $ $ \\
		& $\beta_{\text{numberOfTransfers}}$ & $-0.17$ & $ $ \\
		& $\beta_{\text{inVehicleTime}}$ & $-0.017$ & $[\text{min}^{-1}]$ \\
		& $\beta_{\text{transferTime}}$ & $-0.0484$ & $[\text{min}^{-1}]$ \\
		& $\beta_{\text{accessEgressTime}}$ & $-0.0804$ & $[\text{min}^{-1}]$ \\
		\hline
		Bike & $\alpha_{\text{bike}}$ & $0.1$ & $ $ \\
		& $\beta_{\text{travelTime,bike}}$ & $-0.15$ & $[\text{min}^{-1}]$ \\
		& $\beta_{\text{age,bike}}$ & $-0.0496$ & $[\text{a}^{-1}]$ \\
		\hline
		Walk & $\alpha_{\text{walk}}$ & $1.43$ & $ $ \\
		& $\beta_{\text{travelTime,walk}}$ & $-0.09$ & $[\text{min}^{-1}]$ \\
		\hline
		Others & $\beta_{\text{cost}}$ & $-0.206$ & $[\text{EUR}^{-1}]$ \\
		& $\lambda$ & $-0.4$ & $ $ \\
		& $\theta_{\text{averageCrowflyDistance}}$ & $40$ & $[\text{km}]$ \\
		\hline
		Calibration & $\theta_{\text{parkingSearchPenalty}}$ & $4$ & $[\text{min}]$ \\
		& $\theta_{\text{accessEgressWalkTime}}$ & $4$ & $[\text{min}]$ \\
		\hline\hline
	\end{tabular}
\end{table}

\begin{equation}\begin{aligned}
u_{\text{car}}(x) &=
\alpha_{\text{car}} \\
&+ \beta_{\text{travelTime,car}} \cdot x_{\text{travelTime,car}} \\
&+ \beta_{\text{travelTime,car}} \cdot \theta_{\text{parkingSeachPenalty}} \\
&+ \beta_{\text{travelTime,walk}} \cdot \theta_{\text{accessEgressWalkTime}} \\
&+ \beta_{\text{cost}} \cdot \left(\frac{x_{\text{crowflyDistance}}}{\theta_{\text{averageDistance}}}\right)^\lambda \cdot x_{\text{cost,car}}
\label{eq:mode_choice_model_car}
\end{aligned}\end{equation}

\begin{equation}\begin{aligned}
u_{\text{pt}}(x) &=
\alpha_{\text{pt}} \\
&+ \beta_{\text{numberOfTransfers}} \cdot x_{\text{numberOfTransfers}} \\
&+ \beta_{\text{inVehicleTime}} \cdot x_{\text{inVehicleTime}} \\
&+ \beta_{\text{transferTime}} \cdot x_{\text{transferTime}} \\
&+ \beta_{\text{accessEgressTime}} \cdot x_{\text{accessEgressTime}} \\
&+ \beta_{\text{cost}} \cdot \left(\frac{x_{\text{crowflyDistance}}}{\theta_{\text{averageDistance}}}\right)^\lambda \cdot x_{\text{cost,pt}}
\label{eq:mode_choice_model_pt}
\end{aligned}\end{equation}

\begin{equation}\begin{aligned}
u_{\text{bike}}(x) &= \alpha_{\text{bike}} \\
&+ \beta_{\text{travelTime,bike}} \cdot x_{\text{travelTime,bike}} \\
&+ \beta_{\text{age,bike}} \cdot \text{max}\left( 0\ , a_{\text{age}} - 18 \right)
\label{eq:mode_choice_model_bike}
\end{aligned}\end{equation}

\begin{equation}\begin{aligned}
u_{\text{walk}}(x) &= \alpha_{\text{walk}} \\
&+ \beta_{\text{travelTime,walk}} \cdot x_{\text{travelTime,walk}}
\label{eq:mode_choice_model_walking}
\end{aligned}\end{equation}

Unfortunately, at the time of writing, the authors do
not have access to an extended version of the household travel survey which includes geo-coded
start and end locations of observed trips. Since without that information
no unchosen alternatives can be examined, no specific model parameters for
the \^Ile-de-France region could be estimated. Instead, previously presented parameters and
models for the city of Zurich \cite{choice_models_trb, astra_report} have been used as starting values. The
model parameters (with only slight modifications necessary) have then been calibrated
such that a number of distributions in the simulation match well the observed
reference data: travel times and distances for each mode, mode shares in total,
by distance classes and by time of day. Therefore, all $\alpha$ and $\beta$ parameters
in the model are initially
estimated model parameters that have been adapted to the agent population of Paris.
The strongest modifications have been applied to $\beta_{\text{cost}}$, which determines
the cost sensitivity of the population. As a first sanity check it can be stated
that the value of time in the calibrated model (calculated as $\beta_{\text{travelTime,car}} / \beta_{\text{cost}}$)
of around 19.40 EUR/h falls well into the range that has been reported previously for \^Ile-de-France \cite{MEUNIER201562}.

While a thorough presentation of the validation results for the synthetic
population and the choice model exceeds the scope of this paper, we would like
to refer the reader to upcoming work \cite{paris_documentation}, which will detail the steps  briefly mentioned
above and provide more in-depth comparison with reference data.

\subsection{Future Scenarios}

The aim of the future scenarios is to obtain an idea about the demand for AMoD
travel in Paris. For that purpose, first an operating area needs to be chosen.
For the sake of simplicity, in this research, only the city area of Paris within
the highway ring is served by a fleet of automated single-occupancy taxis
(see Figure \ref{fig:map}). Future studies may be based on a more sensible choice
of operating area as will be explained below. Furthermore, our simulations assume
that the vehicles are electric, which has an impact on the cost of the service.

Three experiments
are set up. In the first experiment, a best-response model is configured in such a
way that all trips that \textit{can} be served by the automated taxi service \textit{must}
be served by it. That means regardless of waiting times or travel costs, any trips
in the daily plans of all travellers that are not otherwise constrained are converted
to the AMoD transport mode.
Obviously, the experiment assumes that the service is so attractive that it is
used in any possible case. The simulation yields therefore an estimate of the
fleet size that is needed to serve the ``maximum static demand scenario''.

Second, the simulation is run with a multinomial logit model with the additional
AMoD transport mode. The parameters of the utility function are derived from
existing parameters for public transport since at the time
of conducting the experiments no stated choice data or similar is available to
to the authors. The utility function is defined as follows:

\begin{equation}\begin{aligned}
u_\text{AMoD} &= \alpha_\text{pt} + \beta_\text{travelTime,pt} \cdot x_\text{travelTime,av} \\
&+ \beta_\text{transferTime,pt} \cdot x_\text{waitingTime,av} \\
&+ \beta_\text{cost} \cdot p_\text{av} \cdot x_\text{networkDistance,av}
\label{eq:av_utility}
\end{aligned}\end{equation}

The waiting times are based on moving average estimates over 10 iterations. Spatially,
they are estimated on the basis of IRIS zones. Here, $p_{av}$ denotes the price per
kilometre, which is fixed to various values in a number of experiments. The experiment
therefore has the purpose to explore the resulting demand in
the system given a dependency of the choice behaviour on the level of service, a
fixed fleet size and a fixed price level.

The third experiment makes use of a detailed cost model of automated mobility \cite{costpaper}.
It has been successfully applied to the case of Zurich, Switzerland and has recently been
applied to a range of cities worldwide \cite{int_costpaper}. Depending on the fleet
utilization, empty distance, fleet sizes, among other components, the cost model calculates
a passenger price per kilometre that covers the expenses of maintaining the service. In the present simulation setup those prices are
calculated in each iteration, while a moving average of the previous 10 iterations
is fed into the choice model as described in Equation \ref{eq:av_utility}. The cost model itself is specified by maintenance costs,
interest costs and a multitude of additional factors. Unfortunately, to date, no
specific assumptions for Paris are available. Therefore, to present the methodology
for a fully dynamic demand estimate, the model specification for Berlin is used,
which should be closest to the case of Paris among the available city-specific parameter sets.
In the present case, the cost model is configured such that the service should always have zero net costs, i.e.
no profit margin is included.

It is important to mention that in all experiments trips that are performed
using the ``walk'' or ``bike'' transport mode in the baseline scenario are never
converted to AMoD trips. Furthermore, constant travel times within 5 minute bins on all network
links are enforced as measured from the baseline scenario. Therefore, effects in
travel time changes due to fewer or more vehicles on the road are deliberately ignored
here. This way, we avoid to motivate additional assumptions on flow efficiency effects of
automated vehicles in the current stage of this research.

All simulations are run using a 10\% sample of the full agent population. This
way, feasible simulation times of around 5h per simulation on a modern cluster
are achieved.
