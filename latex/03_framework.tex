\section{Simulation Framework}

As can be seen from the previous
section, agent-based models are commonly applied to the simulation of AMoD systems
because of their ability to model individual trips and interaction of entities such
as travellers and taxis. For the study at hand the agent-based transport simulation
framework MATSim has been chosen. In the next section a short introduction to
MATSim will be given, followed by a more detailed discussion of a number of
extensions that have been made to the system.

\subsection{MATSim}

A typical MATSim simulation needs a population of travellers
as an input. It contains a large number of traveller agents with sociodemographics
and detailed daily plans. An agent may have multiple activities throughout one
day that should be conducted at a certain time for a certain duration. Those
activities are connected by trips, which are assigned a certain mode of transport.

For the simulation of road-based traffic MATSim provides a network simulation
component which works in a timestep- and queue-based manner. Depending on when
agents enter certain road segments they may be delayed due to emergent traffic
jams or get slowed down because of busses or other vehicles blocking the road.

For the present work, mainly the convenient data structures of MATSim, as well as
the network simulation are used. On top of that, standard MATSim also provides
an ``evolutionary learning loop'' in which agents adapt their daily plans to
the given transport system and other agents' decisions. In this work, a different
approach is used which applies a discrete mode choice model directly to modify
agents' plans.

\subsection{Discrete mode choice model}

In standard MATSim, agents learn by applying random modifications to their current
plan, assigning a score as soon as it has been simulated in the next iteration
of the simulated day and comparing this score to previously tested plans. By always
keeping (and selecting between) a number of plans as the basis for the next iteration
and by always removing the worst ones, each agent optimizes its own plan until
a stochastic user equilibrium is reached. Unfortunately, this process can be
rather slow since improvements happen purely by chance and the probability that
highly unlikely plans are generated for testing (such as walking to work for 50km) is high.

For these and other reasons, work has recently been put into using existing discrete
choice models within the MATSim loop to make decisions \cite{choice_models_abmtrans,choice_models_trb}. The way the
system works (Figure \ref{fig:loop}) is that choice dimensions such as travel times, waiting times and others are estimated from the latest network simulation and/or previous iterations. At the end of each cycle a configurable share of agents applies a given mode choice model to their plans.
Subsequently, the state of the transport system will change and different decisions may
be taken in the next iterations. Ultimately, mode shares stabilize and go into
equilibrium with the traffic conditions in the network.

The framework makes it easy to switch between different model formulations. Currently,
utility functions can be used in a best-response context or as part of a multinomial
logit model\cite{train2009discrete}. Also, a purely random selection of choice
alternatives can be configured. In any case, the choice set available to an agent
is restricted by a number of constraints, the most important being the ``vehicle continuity
constraint''. It defines that an agent is only allowed to choose a certain mode
of transport if the respective vehicle has been moved to the origin of the trip
in question. For purely trip-based models, the considered choice sets consist of
single mode options for a specific trip, but tour-based or whole day-based choice sets can
be generated to allow for richer, but computationally more expensive decisions.

\begin{figure}
\centering
\includegraphics[width=3.5in]{images/loop.pdf}
\caption{Iterative structure of the simulation: First, the synthetic population
is simulated (in combination with automated taxis). Second, predictions for the
updated choices of the travellers are made. Third, based on the prediction a
mode choice model is used to arrive at new daily travel plans.}
\label{fig:loop}
\end{figure}

\subsection{Simulation of automated vehicles}

For the simulation of automated vehicles an extension to MATSim \cite{horl_av,dvrp} is
used. Each vehicle is simulated independently in the network. A global dispatching
component controls the movement of fleet vehicles and manages the assignment
of requests. Those requests are created online, i.e. as soon as a traveller
wants to depart at a certain location. The dispatcher then uses a heuristic
algorithm developed in \cite{heuristic}: In case there are more unoccupied vehicles
than pending requests in the system the closest vehicle to any incoming request is assigned to it. If the amount of requests exceeds the amount of available vehicles
a vehicle is assigned to its closest request as soon as it becomes available.
The vehicle then drives through the network to pick up the customer and subsequently moves the
passenger to its destination. No re-assignments of requests and vehicles while
approaching the customer are taking place in this approach.

It must be noted that the heuristic approach is optimized for simulation speed,
while other algorithms may produce better results. In fact, previous studies
\cite{amod_zh} have shown that more computationally expensive algorithms may
outperform the heuristic, especially with regards to specific objectives such
as minimizing overall customer waiting time or minimizing fleet empty mileage.
Many of such algorithms have been developed in the AMoDeus framework \cite{amodeus}, which adds
an open easy-to-use fleet management benchmarking layer on top of the code used here.
